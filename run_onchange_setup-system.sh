#!/usr/bin/env sh

# Switch capslock.escape
setxkbmap -option caps:swapescape

# Update font cache
fc-cache

# Set shell
chsh -s /bin/zsh
