#!/usr/bin/env sh

sudo apt install -y bat curl direnv exa feh fd-find i3 pass ripgrep tldr zsh
sudo apt install autoconf bison patch build-essential libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libgmp-dev libncurses5-dev libffi-dev libgdbm6 libgdbm-dev libdb-dev uuid-dev

# Install Doom Emacs
if command -v snap && ! snap list emacs
then
    snap install emacs --classic
fi

if [ ! -d "$HOME/.emacs.d" ]
then
    git clone --depth 1 https://github.com/doomemacs/doomemacs ~/.emacs.d
    ~/.emacs.d/bin/doom install
fi

# Install ASDF
if [ ! -d "$HOME/.asdf" ]
then
    git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.11.2
fi

# Symlink bat
if [ ! -f "$HOME/bin/bat" ]
then
    ln -s /usr/bin/batcat ~/bin/bat
fi
