[Appearance]
BoldIntense=true
ColorScheme=DoomOne
Font=FantasqueSansMono Nerd Font,12,-1,5,50,0,0,0,0,0

[General]
Name=Profile
Parent=FALLBACK/

[Scrolling]
HighlightScrolledLines=false
ScrollBarPosition=2
