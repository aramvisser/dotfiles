[Appearance]
BoldIntense=true
ColorScheme=DoomOne
Font=FantasqueSansMono Nerd Font,11,-1,5,50,0,0,0,0,0

[General]
LocalTabTitleFormat=%d : %n [scratch]
Name=Scratchpad
Parent=FALLBACK/
RemoteTabTitleFormat=%d : %n [scratch]

[Scrolling]
HighlightScrolledLines=false
ScrollBarPosition=2
